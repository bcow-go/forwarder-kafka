package kafka

type (
	Option struct {
		Topics         []string
		FlushTimeoutMs int
		ConfigMap      *ConfigMap
		ErrorHandler   ErrorHandlerFunc
	}
)
