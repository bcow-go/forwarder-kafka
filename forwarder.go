package kafka

import (
	"fmt"
	"log"
	"net"
	"os"
	"strings"
	"time"

	"gopkg.in/confluentinc/confluent-kafka-go.v1/kafka"
)

const (
	KAFKA_CONF_BOOTSTRAP_SERVERS = "bootstrap.servers"
)

var (
	defaultLogger = log.New(os.Stdout, "[bcow-go/forwarder-kafka]", log.LstdFlags|log.Lmicroseconds|log.Llongfile|log.Lmsgprefix)
)

type (
	Producer       = kafka.Producer
	ConfigMap      = kafka.ConfigMap
	Message        = kafka.Message
	TopicPartition = kafka.TopicPartition
	Error          = kafka.Error
	ErrorCode      = kafka.ErrorCode

	ErrorHandlerFunc func(err kafka.Error) (disposed bool)
)

type Forwarder struct {
	producer *kafka.Producer

	errorHandler   ErrorHandlerFunc
	flushTimeoutMs int
	pingTimeout    time.Duration
	logger         *log.Logger
}

func NewForwarder(opt *Option) *Forwarder {
	instance := &Forwarder{
		errorHandler:   opt.ErrorHandler,
		flushTimeoutMs: opt.FlushTimeoutMs,
		logger:         defaultLogger,
	}
	instance.initProducer(opt.ConfigMap)
	instance.listenEvents()
	return instance
}

func (f *Forwarder) Write(topic string, key, value []byte) error {
	return f.WriteMessage(
		&kafka.Message{
			TopicPartition: kafka.TopicPartition{Topic: &topic, Partition: kafka.PartitionAny},
			Key:            key,
			Value:          value,
		})
}

func (f *Forwarder) WriteMessage(message *Message) error {
	p := f.producer
	// Wait for message deliveries before shutting down
	err := p.Produce(message, nil)
	if err != nil {
		return err
	}
	p.Flush(f.flushTimeoutMs)
	return err
}

func (f *Forwarder) Close() {
	p := f.producer

	defer p.Close()
}

func (f *Forwarder) Runner() *Runner {
	return &Runner{
		forwarder: f,
	}
}

func (f *Forwarder) listenEvents() {
	p := f.producer

	// Delivery report handler for produced messages
	go func() {
		for ev := range p.Events() {
			for {
				switch e := ev.(type) {
				case kafka.Error:
					switch e.Code() {
					case kafka.ErrUnknownTopic, kafka.ErrUnknownTopicOrPart:
						if !f.handleKafkaError(e) {
							log.Fatalf("[bcow-go/forwarder-kafka] %% Error: (%#v) %+v: %v\n", e.Code(), e.Code(), e)
						}

					/* NOTE: https://github.com/edenhill/librdkafka/issues/64
					Currently the only error codes signaled through the error_cb are:
					- RD_KAFKA_RESP_ERR__ALL_BROKERS_DOWN - all brokers are down
					- RD_KAFKA_RESP_ERR__FAIL - generic low level errors (socket failures because of lacking ipv4/ipv6 support)
					- RD_KAFKA_RESP_ERR__RESOLVE - failure to resolve the broker address
					- RD_KAFKA_RESP_ERR__CRIT_SYS_RESOURCE - failed to create new thread
					- RD_KAFKA_RESP_ERR__FS - various file errors in the consumer offset management code
					- RD_KAFKA_RESP_ERR__TRANSPORT - failed to connect to single broker, or connection error for single broker
					- RD_KAFKA_RESP_ERR__BAD_MSG - received malformed packet from broker (version mismatch?)
					I guess you could treat all but .._TRANSPORT as fatal.
					*/
					case kafka.ErrTransport:
						if !f.handleKafkaError(e) {
							log.Printf("[bcow-go/forwarder-kafka] %% Error: (%#v) %+v: %v\n", e.Code(), e.Code(), e)
						}

					case kafka.ErrAllBrokersDown,
						kafka.ErrFail,
						kafka.ErrResolve,
						kafka.ErrCritSysResource,
						kafka.ErrFs,
						kafka.ErrBadMsg:
						log.Fatalf("[bcow-go/forwarder-kafka] %% Error: (%#v) %+v: %v\n", e.Code(), e.Code(), e)

					default:
						if !f.handleKafkaError(e) {
							log.Printf("[bcow-go/forwarder-kafka] %% Error: (%#v) %+v: %v\n", e.Code(), e.Code(), e)
						}
						return
					}
				case *kafka.Message:
					if e.TopicPartition.Error != nil {
						if err, ok := e.TopicPartition.Error.(*kafka.Error); ok {
							ev = *err
							continue
						}
						if err, ok := e.TopicPartition.Error.(kafka.Error); ok {
							ev = err
							continue
						}
						f.logger.Printf("(%s) (%T): %[2]v\n",
							e.TopicPartition,
							e.TopicPartition.Error)
					}
				default:
					log.Printf("[bcow-go/forwarder-kafka] %% Notice: Ignored %v\n", e)
				}
				break
			}
		}
	}()
}

func (f *Forwarder) handleKafkaError(err kafka.Error) (disposed bool) {
	if f.errorHandler != nil {
		return f.errorHandler(err)
	}
	return false
}

func (f *Forwarder) initProducer(conf *ConfigMap) {
	{
		v, _ := conf.Get(KAFKA_CONF_BOOTSTRAP_SERVERS, nil)
		if v == nil {
			panic(fmt.Sprintf("missing kafka config '%s'", KAFKA_CONF_BOOTSTRAP_SERVERS))
		}
		bootstrapServers := v.(string)
		brokerAddresses := strings.Split(bootstrapServers, ",")
		err := f.ping(brokerAddresses)
		if err != nil {
			log.Fatalf("[bcow-go/forwarder-kafka] %% Error: %+v\n", err)
		}
	}

	p, err := kafka.NewProducer(conf)
	if err != nil {
		log.Fatalf("[bcow-go/forwarder-kafka] %% Error: %+v\n", err)
	}
	f.producer = p
}

func (f *Forwarder) ping(addresses []string) (err error) {
	timeout := f.pingTimeout
	for _, address := range addresses {
		var conn net.Conn
		conn, err = net.DialTimeout("tcp", address, timeout)
		if err != nil {
			continue
		}
		if conn != nil {
			defer conn.Close()
			log.Printf("[bcow-go/forwarder-kafka] tcp %s opened\n", address)
			return nil
		}
	}
	return
}
