package test

import (
	"fmt"
	"os"
	"testing"
	"time"

	kafka "gitlab.bcowtech.de/bcow-go/forwarder-kafka"
)

var (
	BootstrapServers = os.Getenv("BOOTSTRAP_SERVERS")
)

func TestForwarder(t *testing.T) {
	forwarder := kafka.NewForwarder(&kafka.Option{
		FlushTimeoutMs: int(15 * (time.Second / time.Millisecond)),
		ConfigMap: &kafka.ConfigMap{
			/*
			 * you may add the following config:
			 *  bootstrap.servers
			 *  enable.idempotence
			 *  message.send.max.retries
			 *  partitioner
			 *  compression.type
			 *  security.protocol
			 *  request.required.acks   ; must greater than min.insync.replicas
			 */
			"client.id":                "demo",
			"bootstrap.servers":        BootstrapServers,
			"enable.idempotence":       "true",
			"message.send.max.retries": "15",
		},
		ErrorHandler: func(err kafka.Error) (disposed bool) {
			if err.Code() == kafka.ErrAllBrokersDown {
				return true
			}
			return false
		},
	})

	defer forwarder.Close()

	for _, word := range []string{"Welcome", "to", "the", "Confluent", "Kafka", "Golang", "client"} {
		forwarder.Write("myTopic", nil, []byte(word))
		time.Sleep(2 * time.Second)
		fmt.Println(word)
	}
}
