package kafka

type Runner struct {
	forwarder *Forwarder
}

func (r *Runner) Start() {
	r.forwarder.logger.Printf("[bcow-go/forwarder-kafka] Started\n")
}

func (r *Runner) Stop() {
	logger := r.forwarder.logger
	logger.Printf("[bcow-go/forwarder-kafka] Closing\n")
	r.forwarder.Close()
	logger.Printf("[bcow-go/forwarder-kafka] Closed\n")
}
